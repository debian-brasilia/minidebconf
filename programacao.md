---
layout: minidebconf
title: Programação
---

<!-- A programação do evento será disponibilizada na semana que antecede o evento. Enquanto isso, aproveite para se inscrever para o evento e submeter atividades, caso tenha interesse. -->


{% for minidebconf_schedule in site.data.schedule %}

---

## {{ minidebconf_schedule.day }} de Maio

<div class="card">
  <div class="table-responsive">
    <table class="table align-items-center mb-0">
      <thead>
        <tr>
          <th class="text-uppercase text-dark text-xxs font-weight-bolder opacity-7">Horário</th>
          {% for row_name in minidebconf_schedule.rows %}
          <th class="text-uppercase text-dark text-xxs font-weight-bolder opacity-7">{{ row_name }}</th>
          {% endfor %}
        </tr>
       </thead>
       <tbody>
          {% for schedule in minidebconf_schedule.schedule %}
          <tr>
            {% if schedule.time_disabled  == true %}
            {% else %}
            <td {% if schedule.time_rowspan %}rowspan="{{ schedule.time_rowspan }}"{% endif %}>
              <h6 class="mb-0 text-xs">{{ schedule.time }}</h6>
            </td>
            {% endif %}
            {% if minidebconf_schedule.talk %}
              {% if schedule.talk_disabled  == true %}
              {% else %}
              <td {% if schedule.talk_rowspan %}rowspan="{{ schedule.talk_rowspan }}"{% endif %}>
                {% if schedule.talk_summary %}
                  {% if schedule.talk_skip_details %}
                  <p class="text-xs font-weight-bold mb-0">{{ schedule.talk_title }}{% if schedule.talk_slides %}<a href="{{ schedule.talk_slides }}" target="_blank" class="fa fa-cloud-download ml-4"  data-toggle="tooltip" data-placement="top" title="Slides" aria-hidden="true"></a>{% endif %}{% if schedule.talk_video %}<a href="{{ schedule.talk_video }}" target="_blank" class="fa fa-video-camera ml-2"  data-toggle="tooltip" data-placement="top" title="Vídeo" aria-hidden="true"></a>{% endif %}
                    {% if schedule.talk_authors %}
                    <p class="text-xs mb-0">{{ schedule.talk_authors }}</p>
                    {% endif %}
                  </p>
                  <p class="text-xs text-light mb-0">---<br>{{ schedule.talk_summary | markdownify | newline_to_br }}</p>
                  {% else %}
                  <details>
                    <summary class="text-xs font-weight-bold mb-0">{{ schedule.talk_title }}{% if schedule.talk_slides %}<a href="{{ schedule.talk_slides }}" target="_blank" class="fa fa-cloud-download ml-4"  data-toggle="tooltip" data-placement="top" title="Slides" aria-hidden="true"></a>{% endif %}{% if schedule.talk_video %}<a href="{{ schedule.talk_video }}" target="_blank" class="fa fa-video-camera ml-2"  data-toggle="tooltip" data-placement="top" title="Vídeo" aria-hidden="true"></a>{% endif %}
                    {% if schedule.talk_authors %}
                    <p class="text-xs mb-0">{{ schedule.talk_authors }}</p>
                    {% endif %}
                    </summary>
                    <p class="text-xs text-light mb-0">---<br>{{ schedule.talk_summary | markdownify | newline_to_br }}</p>
                  </details>
                  {% endif %}
                {% else %}
                <p class="text-xs font-weight-bold mb-0">{{ schedule.talk_title }}{% if schedule.talk_slides %}<a href="{{ schedule.talk_slides }}" target="_blank" class="fa fa-cloud-download ml-4"  data-toggle="tooltip" data-placement="top" title="Slides" aria-hidden="true"></a>{% endif %}{% if schedule.talk_video %}<a href="{{ schedule.talk_video }}" target="_blank" class="fa fa-video-camera ml-2"  data-toggle="tooltip" data-placement="top" title="Vídeo" aria-hidden="true"></a>{% endif %}
                  {% if schedule.talk_authors %}
                  <p class="text-xs mb-0">{{ schedule.talk_authors }}</p>
                  {% endif %}
                </p>
                {% endif %}
              </td>
              {% endif %}
            {% endif %}
            {% if minidebconf_schedule.workshop %}
              {% if schedule.workshop_rowspan %}
              <td rowspan="{{schedule.workshop_rowspan}}">
                {% if schedule.workshop_title %}
                <details>
                  <summary class="text-xs font-weight-bold mb-0">{{ schedule.workshop_title }}
                  {% if schedule.workshop_authors %}
                    <p class="text-xs mb-0">{{ schedule.workshop_authors }}</p>
                  {% endif %}
                  </summary>
                  <p class="text-xs text-light mb-0">---<br>{{ schedule.workshop_summary | markdownify | newline_to_br }}</p>
                </details>
                {% endif %}
              </td>
              {% endif %}
            {% endif %}
          </tr>
          {% endfor %}
       </tbody>
     </table>
   </div>
</div>

---

{% endfor %}
