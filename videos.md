---
layout: minidebconf
title: Vídeos
---

---

### 25 de maio

#### Parte 1

<div class="row">
    <div class="col-12">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/8A_Lftn4Kdk"></iframe>
        </div>
    </div>
</div>

#### Parte 2

<div class="row">
    <div class="col-12">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/3VmCCSBOG68"></iframe>
        </div>
    </div>
</div>


---

### 26 de maio

<div class="row">
    <div class="col-12">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/AXNELyL1meY"></iframe>
        </div>
    </div>
</div>

