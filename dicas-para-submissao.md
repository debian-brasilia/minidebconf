---
layout: minidebconf
title: Dicas para Submissão de Atividades
---

Em todas as edições da MiniDebConf, as estrelas são sempre os membros da comunidade que se dispõem a compartilhar seu conhecimento sobre Debian com os demais.

Listamos algumas dicas para ajudá-lo a enviar uma boa proposta e, se for aprovada, fazer uma boa apresentação.

<h4><a href="/submissao-de-atividades">Envie sua submissão</a></h4>

<h4></h4>

### 1. Escolha um bom tema

Parece uma dica óbvia, mas o que caracteriza um bom tema? O tema é o principal fator para sua proposta ser aprovada. Veja as dicas abaixo:

- Um assunto com o qual você se sinta confortável. Você não precisa ser um expert, apenas se sentir seguro sobre o tema. Sabemos que a insegurança pode ser um impeditivo, veja mais sobre isso abaixo.
- Algo relacionado ao Debian. Algumas submissões que recebemos, embora sejam ótimas propostas, não tem relação com o Debian. Você pode falar sobre contribuição para o Software Livre, relacionando aos temas. Você pode falar de softwares livres, usando o Debian como sistema operacional.
- Não precisa ser só sobre empacotamento. As MiniDebConfs são eventos para todos que usam Debian, não só para Desenvolvedores. Palestras sobre instalação, uso, bugs, documentação, suporte, publicidade, marketing, organização de eventos, produção de material gráfico, design, tradução, e tudo mais que envolva o Debian são muito bem-vindas. Estes temas inclusive costumam ter menos concorrência para quem quer falar.
- Respeite o <a href="/codigo-de-conduta">código de conduta</a>. Temas que promovam discriminação ou ofensas de qualquer tipo, ou que tenham o objetivo de vender um produto ou serviço, ou divulgar sua empresa, não serão aceitos.
- Na dúvida entre 2 ou mais assuntos? Mande todos Smile 18px. Não há limite para a submissão de atividades. Pode enviar mais de uma que nós avaliaremos cada proposta individualmente da mesma forma.

### 2. Descreva bem a sua atividade e o seu minicurrículo

A avaliação das submissões é feita apenas com base na sua descrição, já que não pedimos que envie os slides neste momento. Por isso, descreva sua apresentação o melhor possível para que possamos entender o que você vai apresentar e como isso vai contribuir com as pessoas que vão assistir. No minicurrículo, fale quem você é, suas experiências e se já palestrou antes.

### 3. Vença a barreira da insegurança. O que você tem para compartilhar tem valor!

Nós sabemos que a insegurança pode te fazer pensar que você não sabe o suficiente. Então faça o teste abaixo para saber se você está pronto para palestrar em uma MiniDebConf.

- Você compartilha coisas que você aprende com seus amigos ou colegas de trabalho?
- Você já ensinou alguém a fazer algo novo no Debian?
- Você tem uma case legal sobre Debian?
- Você criou um pacote, tradução, ou uma forma diferente de fazer algo no Debian?
- Você vê algum problema ou erro que atrapalham as pessoas e tem uma sugestão de solução para isso?

Se você respondeu SIM para qualquer uma destas questões, parabéns você pode enviar sua proposta de atividade!

Se o seu problema é a timidez de falar em público e se sua proposta for aprovada, saiba que:

- Você pode fazer uma sessão de mentoria com os organizadores da MiniDebConf para tirar suas dúvidas e receber dicas sobre como se apresentar.
- A comunidade Debian é super receptiva.
- Nosso <a href="/codigo-de-conduta">código de conduta</a> promove um ambiente saudável para todos, inclusive para os palestrantes. Por isso você terá todo o suporte da equipe para o que precisar.
- Todo mundo fica ansioso em algum nível na hora de se apresentar, até os mais experientes. Não se intimide com o frio na barriga, pois a sensação de palestrar em uma MiniDebConf é ótima (pergunte para quem já experimentou.

### Não perca o prazo de submissão! Somente até o dia {{ site.submission-close-date }}

Não deixe para a última hora. Mande sua(s) proposta(s) agora mesmo e aguarde a divulgação dos resultados no dia {{ site.submission-results-date }}.

<h4><a href="/submissao-de-atividades">Envie sua submissão</a></h4>

Se você, ainda assim, não quer palestrar, não tem problema. <a href="/inscricao">Inscreva-se como participante</a>. Conhece alguém que se encaixa nesse perfil? Compartilhe com esta pessoa.

Vemos vocês lá!
