---
layout: minidebconf
title: Submissão de Atividades
---

{% if site.submission-is-open %}

A submissão de atividades para a {{ site.title }} {{ site.year }} será aberta em {{ site.submission-open-date }}.

### Tópicos

Em princípio, qualquer tópico relacionado a software livre está dentro do escopo do evento. No entanto, será dada prioridade a tópicos relacionados ao projeto Debian, e dentre estes, será dada prioridade a tópicos relacionados a contribuição com o projeto.

### Diversidade

A MiniDebConf é um evento comprometido com a diversidade. Gostaríamos muito de receber propostas de atividades de pessoas que fazem parte de grupos com pouca representação na comunidade de software livre e que gostariam de falar sobre algo relacionado ao Debian. Se você faz parte de um desses grupos, nos envie sua proposta. Se você conhece alguém que faz parte de um desses grupos, incentive essa pessoa a enviar uma proposta de atividade.

Se você gostaria de palestrar sobre Debian, mas não tem certeza sobre o que poderia falar, entre em contato ({{ site.email }}) que podemos te ajudar.

### Tipos de atividade
Você pode enviar 4 tipos de atividades:

- Lightning talk (5 minutos)
- Painel (1 hora)
- Palestra:
    - Longa (1 hora)
    - Curta (30 minutos)
- Oficina (1 a 3 horas - especificar no resumo)

### Dicas

Veja algumas <a href="/dicas-para-submissao">dicas</a> que poderão te ajudar no envio das atividades:

### Datas importantes

- Limite para submissão: {{ site.submission-close-date }}.
- Notificação do resultado da seleção aos proponentes: {{ site.submission-results-date }}.

---

## Submissão

Para enviar sua proposta de atividade preencha o formulário a seguir e clique no botão __Submeter Palestra__ ao final.

<form class="mt-4" method="post" action="https://minidebconfbrasilia.arthurbdiniz.com/api/v1/talk">
    <div class="form-group">
        <label for="example-text-input" class="form-control-label">Autores</label>
        <input class="form-control" type="text" value="" placeholder="nome (nome@example.com), outro-nome (outro-nome@example.com)" id="autores" name="autores" required>
    </div>

    <div class="form-group">
        <label for="example-text-input" class="form-control-label">Título</label>
        <input class="form-control" type="text" value="" id="titulo" name="titulo" required>
    </div>

    <div class="form-group">
        <label for="example-number-input" class="form-control-label">Tipo</label>
        <select class="form-control" id="tipo" name="tipo">
            <option>Lightning talk (5 minutos)</option>
            <option>Painel (1 hora)</option>
            <option>Palestra Curta (30 minutos)</option>
            <option>Palestra Longa (1 hora)</option>
            <option>Workshop/Oficina (~1-3 horas)</option>
        </select>
    </div>

    <div class="form-group">
        <label for="exampleFormControlTextarea1">Resumo</label>
        <textarea class="form-control" id="resumo" placeholder="" rows="5" name="resumo" required></textarea>
        <small>Escreva dois ou três parágrafos descrevendo sua palestra. Quem é o seu público? O que eles vão ganhar com isso? O que você vai cobrir?</small>
    </div>

    <hr/>

    <div>
        <div class="form-check form-switch">
            <input class="form-check-input" type="checkbox" id="video" name="video" required>
            <label class="form-check-label" for="flexSwitchCheckDefault">Video</label>
        </div>
        <small>Ao marcar isso, você está dando permissão para que a palestra seja filmada e distribuída pela conferência, sob a licença DebConf Video License (semelhante ao MIT).</small>
    </div>

    <hr/>

    <div class="mt-5">
        <input type="submit" class="btn btn-primary" value="Submeter Palestra">
    </div>

</form>

{% else %}

##### A submissão de atividades está fechada.

{% endif %}
