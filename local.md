---
layout: minidebconf
title: Local
---

---


## 25 e 26 de maio

A **{{ site.title }} {{ site.year }}** acontecerá na **{{ site.mdc-event-location }}** entre os dias **25 e 26 de maio**.

A {{ site.mdc-event-location }} foi criada no início dos anos 1990, um grupo de professores da **Faculdade de Tecnologia da Universidade de Brasília** se uniu em torno de uma necessidade em comum: superar obstáculos burocráticos para se fazer pesquisa acadêmica.

Surgiu assim, em 13 de março de 1992, a Fundação de Empreendimentos Científicos e Tecnológicos – a Finatec!

Para mais informações, consulte o site da {{ site.mdc-event-location }}: {{ site.mdc-event-location-site }}.


- Endereço: **Fundação de Empreendimentos Científicos e Tecnológicos (FINATEC), Campus Universitário Darcy Ribeiro (UnB), Asa Norte, Brasília, DF**
- CEP: **70910-900**


<div class="embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-47.874732613563545%2C-15.775822458859468%2C-47.86765158176422%2C-15.771134985950658&amp;layer=mapnik&amp;marker=-15.773478735945721%2C-47.87119209766388" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=-15.77348&amp;mlon=-47.87119#map=18/-15.77348/-47.87119">View Larger Map</a></small>
</div>


---

## 27 de maio


No dia **27 de maio (sábado)** o evento acontecerá no **Esplanada Brasília Hotel**

- Endereço: **SHS QUADRA 03 BL E - Brasília, DF, Brasil**
- CEP: **70322-906**
- Telefone: **+55 61 3535-7000**
