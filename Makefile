build:
	docker build -t debian-brasilia-minidebconf --network host .

run:
	docker run -it -v ${shell pwd}:/site --network host debian-brasilia-minidebconf

shell:
	docker run -it -v ${shell pwd}:/site --network host --entrypoint bash debian-brasilia-minidebconf
