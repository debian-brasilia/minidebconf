---
layout: minidebconf
title: Sobre
---

De {{ site.start }} a {{ site.end }} de {{ site.month }}, {{ site.city }} sediará pela primeira vez uma MiniDebConf, a qual é um evento composto por palestras, oficinas, sprints, BSP (Bug Squashing Party) e eventos sociais.

A {{ site.title }} {{ site.year }} é um evento aberto a todos(as), independente do seu nível de conhecimento sobre Debian. O mais importante será reunir novamente a comunidade para celebrar o maior projeto de Software Livre no mundo, por isso queremos receber desde usuários(as) inexperientes que estão iniciando o seu contato com o Debian até Desenvolvedores(as) oficiais do projeto. Ou seja, estão todos(as) convidados(as)!

As MiniDebConfs são encontros locais organizados por membros do Projeto Debian para atingir objetivos semelhantes aos da DebConf, mas em um contexto regional. Durante todo o ano são organizadas MiniDebConfs ao redor do mundo, como se pode ver <a href="https://wiki.debian.org/MiniDebConf">nesta página</a>.


### Data: {{ site.start }} a {{ site.end }} de {{ site.month }} {{ site.year }}

{{ site.mdc-start-day }} a {{ site.mdc-end-day }} ({{ site.mdc-start-day-weekday }} a {{ site.mdc-end-day-weekday }}): MiniDebConf propriamente dita - palestras, debates, oficinas, e mais "mão na massa"

{{ site.mdc-hacking-day }} ({{ site.mdc-hacking-day-weekday }}): Hacking day - período para colaboradores(as) do Debian se encontrarem e trabalharem conjuntamente em um ou mais aspectos do projeto. Essa será a nossa versão da brasileira da Debcamp, que acontece tradicionalmente antes da Debconf. Não haverá palestras, debates e oficinas nesses dias, teremos apenas "mão na massa" como empacotamentos de softwares, traduções e BSP - Bug Squashing Party :-)

{% if site.mdc-daytrip %}
{{ sitemdc-.daytrip-day }} ({{ site.mdc-daytrip-day-weekday }}) Daytrip - passeio pela cidade e almoço de confraternização.
{% endif %}

### Edições anteriores

Em 2015 aconteceu um evento dedicado ao Debian, a primeira edição da MicroDebConf Brasília:

- <a href="https://wiki.debian.org/Brasil/Eventos/MicroDebConfBSB2015/Report">MicroDebConf Brasília 2015</a>
