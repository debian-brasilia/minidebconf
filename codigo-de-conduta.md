---
layout: minidebconf
title: Código de Conduta
---

A {{ site.title }} está comprometida com um ambiente seguro para todos(as) os(as) participantes. Todos(as) os(as) participantes devem tratar todas as pessoas e instalações com respeito e ajudar a criar um ambiente acolhedor. Se você notar um comportamento que não atende a este padrão, por favor denuncie e ajude a manter a {{ site.title }} tão respeitosa quanto esperamos que ela seja.

Se você for assediado(a) ou observou um ambiente desrespeitoso, os(as) organizadores(as) querem ajudar. Por favor entre em contato conosco pelo e-mail {{ site.email }}. Vamos tratar o seu pedido com dignidade e confidencialidade, investigar e tomar as ações apropriadas. Podemos fornecer informações sobre segurança, serviços de emergência, transporte, acomodações alternativas ou qualquer outra coisa que seja necessária. Se a mediação não for bem-sucedida, a {{ site.title }} reserva-se o direito de tomar medidas contra aqueles(as) que não cessarem com os comportamentos inaceitáveis.

Veja o Código de Conduta da {{ site.title }} abaixo e o Código de Conduta do Debian.

### Código de Conduta da {{ site.title }}

A {{ site.title }}, como parte da comunidade Debian como um todo, assume boa fé em todos(as) aqueles(as) que desejam melhorar o Debian. No entanto, outras experiências em outros eventos nos mostraram a necessidade de adotar um código de conduta no qual declaramos o que esperamos de todos(as) os(as) participantes e organizadores(as) durante a {{ site.title }}.

Este código de conduta aplica-se a todos(as) os(as) participantes da {{ site.title }}, além do código de conduta da Debian que se aplica à comunidade Debian como um todo.

#### Declaração de diversidade Debian

O Projeto Debian dá as boas-vindas e encoraja a participação de todas as pessoas.

Não importa como você se identifique ou como os outros te percebam: nós te damos as boas-vindas. Nós damos boas-vindas às contribuições de todas as pessoas, contanto que elas interajam construtivamente com a nossa comunidade.

Embora grande parte do trabalho para o nosso projeto seja de natureza técnica, nós valorizamos e encorajamos contribuições de pessoas com experiência em outras áreas, e lhes damos as boas-vindas em nossa comunidade.

#### Seja excelente um(a) com o(a) outro(a)

A {{ site.title }} está comprometida em oferecer um ambiente seguro para todos(as) os(as) participantes. Todos(as) os(as) participantes devem tratar todas as pessoas e instalações com respeito e ajudar a criar um ambiente acolhedor. Se você notar um comportamento que não atende a este padrão, por favor denuncie e ajude a manter a {{ site.title }} tão respeitosa quanto esperamos que ela seja.

A {{ site.title }} está comprometida com os ideais expressos em nossa declaração de diversidade (acima) e no código de conduta Debian recentemente adotado. Pedimos a todos os nossos membros, palestrantes, voluntários(as), participantes e convidados(as) que adotem esses princípios. Somos uma comunidade diversificada. Às vezes isso significa que precisamos trabalhar ainda mais para garantir que estamos criando um ambiente de confiança e respeito, onde todos(as) que participam se sentem confortáveis e incluídos.

Nós valorizamos a sua participação e apreciamos a sua ajuda na realização desse objetivo.

#### Seja respeitoso(a)

Respeite-se e respeite as outras pessoas. Seja cortês com as pessoas à sua volta. Se alguém indicar que não deseja ser fotografado(a), respeite esse desejo. Se alguém indicar que gostaria de ficar sozinho(a), deixe-o(a). Nossos espaços no evento e espaços on-line podem ser compartilhados com o público em geral; por favor seja atencioso(a) com todos(as) os(as) participantes nesses espaços, mesmo que eles(as) não estejam participando do evento.

#### Seja inclusivo(a)

Por padrão, todo o material de apresentação deve ser adequado para pessoas com idade igual ou superior a 12 anos.

Se você, de forma sensata, achar que alguém pode se sentir ofendido(a) pela sua palestra, por favor indique isso explicitamente na descrição da palestra na submissão. Isso será levado em consideração pela equipe de conteúdo. Caso não tenha certeza se isso se aplica a você, entre em contato com a equipe de conteúdo pelo e-email {{ site.email }}. Por favor note que você é o único responsável se algo for considerado inapropriado e você não entrou em contato previamente com a equipe de conteúdo.

#### Seja consciente

Pedimos a todas as pessoas que estejam conscientes de que não toleraremos intimidação, assédio ou qualquer comportamento abusivo, discriminatório ou depreciativo por qualquer pessoa em qualquer evento do Debian ou em mídia on-line relacionada.

As denúncias podem ser feitas aos(as) organizadores(as), entrando em contato com a mesa de credenciamento ou enviando um e-mail para {{ site.email }}. Todas as denúncias feitas aos(as) organizadores(as) do evento permanecerão confidenciais e serão levadas a sério. A denúncia será tratada de forma adequada e com discrição. Considerando-se apropriado, os(as) organizadores(as) ou moderadores(as) do evento podem tomar as seguintes medidas:

- A pessoa pode ser solicitada a pedir desculpas;
- A pessoa pode ser informada para parar/mudar o seu comportamento para a forma adequada;
- A pessoa pode ser avisada que ações voltadas ao cumprimento do código de conduta serão tomadas se o comportamento continuar;
- A pessoa pode ser convidada a sair imediatamente do local e/ou será proibida de continuar participando de qualquer parte do evento;
- O incidente pode ser reportado às autoridades apropriadas.

#### O que isso significa para mim?

Todos os(as) participantes, incluindo participantes do evento e palestrantes, não devem se envolver em qualquer intimidação, assédio ou comportamento abusivo ou discriminatório.

A seguir está uma lista de exemplos de comportamentos que são considerados altamente inapropriados e não serão tolerados na {{ site.title }}:

- Observações ofensivas, verbais ou escritas, relacionadas ao gênero, orientação sexual, deficiência, aparência física, tamanho corporal, raça ou religião;
- Imagens sexuais ou violentas em espaços públicos (incluindo slides de apresentação);
- Intimidação deliberada;
- Perseguição persistente (stalking) ou ficar seguindo alguém;
- Foto ou filmagem indesejada;
- Interrupção contínua de palestras ou outras atividades;
- Contato físico indesejável ou outras formas de agressão;
- Atenção sexual indesejada;
- Piadas sexistas, racistas ou outras exclusivistas;
- Exclusão injustificada do evento ou de eventos relacionados, com base na idade, gênero, orientação sexual, deficiência, aparência física, tamanho corporal, raça, religião.

Queremos que todos se divirtam em nossos eventos.

#### Perguntas?

Se você não tem certeza sobre qualquer coisa neste Código de Conduta do evento, por favor entre em contato com os organizadores da {{ site.title }} pelo e-mail {{ site.email }}.

Se você deseja relatar uma violação deste Código de Conduta, por favor entre em contato pelo e-mail {{ site.email }}.

#### Prometemos a você

- Vamos ler todas as denúncias e teremos várias pessoas nesse endereço que podem ajudar a investigar e resolver a denúncia.
- Responderemos, por escrito, o mais rápido possível para reconhecer a nossa preocupação e assegurar que o assunto está sendo investigado.
- Dependendo da situação, conversaremos com o denunciante, o denunciado, ou ambos para determinar qual mediação e/ou a ação se faz necessária.
- Dependendo do resultado da investigação e da mediação, nos reservamos o direito de expulsar do local a pessoa que não está em conformidade com o nosso Código de Conduta. O Debian, o comitê organizador da {{ site.title }} e o local em que a {{ site.title }} está sendo realizada não serão responsabilizados pelos custos adicionais decorrentes da expulsão do evento.
