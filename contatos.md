---
layout: minidebconf
title: Contatos
---

Para entrar em contato com a organização da {{ site.title }} {{ site.year }}:

- E-mail: <a href="{{ site.email }}">{{ site.email }}</a>
- Lista pública de discussão: <a href="debian-br-eventos@lists.alioth.debian.org">debian-br-eventos@lists.alioth.debian.org</a>
- Canal IRC: __#debian-br-eventos__ no __irc.debian.org__

### Debian {{ site.city }}

- <a href="https://t.me/debianbrasilia">Debian {{ site.city }} no Telegram</a>

### Debian Brasil

Redes sociais livres e descentralizadas:

- <a href="https://quitter.se/debianbrasil">Debian Brasil GNU Social/StatusNet</a>
- <a href="https://identi.ca/debianbrasil">Debian Brasil no Pump.io</a>
- <a href="https://hub.vilarejo.pro.br/channel/debianbrasil">Debian Brasil no Hubzilla</a>

Redes sociais proprietárias e centralizadas:

- <a href="https://t.me/debianbrasil">Debian Brasil no Telegram</a>
- <a href="https://twitter.com/debianbrasil">Debian Brasil no Twitter</a>
- <a href="https://www.facebook.com/DebianBrasil/">Debian Brasil no Facebook</a>
