---
layout: minidebconf
title: Voluntários
---

Como a {{ site.title }} é um evento organizado por voluntários, sua qualidade depende fortemente do nosso esforço, portanto, toda ajuda é bem-vinda! Se você está se perguntando como e com o que exatamente você pode ajudar, aqui está a lista de algumas das atividades que você pode nos dar uma mão:

- __Fotografia__: Leve sua câmera fotográfica e registre as atividades.
- __Conteúdo__: Durante o evento, poste conteúdo nas redes sociais nos perfis do Debian Brasil e Curitiba Livre (você terá acesso a esses perfis de redes sociais).
- __Secretaria__: Ajude no credenciamento dos participantes no decorrer do evento.
- __Transmissão das Palestras__: Auxiliar na filmagem/transmissão das palestras. Não se preocupe! Estaremos lá para auxiliá-los.
- __Auditório__: Dê suporte aos palestrantes durante o evento, ajudando na utilização de microfones e projetores, cronometrando tempo para término da apresentação, e o que mais for preciso.


Se interessou?
Quer nos ajudar?
Preencha o formulário abaixo e entraremos em contato com você!


<form class="mt-4" method="post" action="https://minidebconfbrasilia.arthurbdiniz.com/api/v1/volunteer">
    <div class="form-group">
        <label for="example-text-input" class="form-control-label">Email</label>
        <input class="form-control" type="email" value="" id="email" name="email" required>
        <small>O email deve ser igual ao email utilizado no formulário de inscrição. Caso ainda não tenha feito sua inscrição <a href="/inscricao">clique aqui</a>.</small>
    </div>
    <div class="form-group">
        <label for="example-number-input" class="form-control-label">Tipo</label>
        <select class="form-control" id="tipo" name="tipo">
            <option>Fotografia</option>
            <option>Conteúdo</option>
            <option>Secretaria</option>
            <option>Auditório</option>
        </select>
        <small>Escolha o tipo de voluntariado que mais te agrada e vamos fazer o possível para alocar em sua escolha.</small>
    </div>
    <hr/>
    <div class="mt-5">
        <input type="submit" class="btn btn-primary" value="Submeter">
    </div>
</form>
