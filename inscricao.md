---
layout: minidebconf
title: Inscrição
---

---

{% if site.registration-is-open %}

### Instruções

Obrigado pelo seu interesse em participar da MiniDebConf Brasília 2023!

Por favor, leia as instruções a seguir cuidadosamente:

- Você não está registrado até clicar no botão submeter ao final formulário.
- A concordância com os termos sanitários da Secretaria de Saúde (SES-DF) para
  a prevenção e combate à COVID-19 e o Código de Conduta são obrigatórios.

---


### Formulário


<form class="mt-4" method="post" action="https://minidebconfbrasilia.arthurbdiniz.com/api/v1/user">
    <div class="form-group">
        <label for="example-text-input" class="form-control-label">Nome</label>
        <input class="form-control" type="text" value="" id="nome" name="nome" required>
        <small>Isso aparecerá em sua etiqueta de nome e em áreas públicas deste site, por exemplo, se você enviar uma palestra.</small>
    </div>

    <div class="form-group">
        <label for="example-text-input" class="form-control-label">Complemento (linha 2)</label>
        <input class="form-control" type="text" value="" id="complemento" name="complemento">
        <small>Pode ser sua empresa, projeto, pronome preferido(nickname) ou qualquer coisa que você queira dizer.</small>
    </div>

    <div class="form-group">
        <label for="example-email-input" class="form-control-label">Email</label>
        <input class="form-control" type="email" value="" id="email" name="email" required>
    </div>

    <div class="form-group">
        <label for="example-tel-input" class="form-control-label">Telefone</label>
        <input class="form-control" type="tel" value="" placeholder="(xx) xxxxx-xxxx" id="telefone" name="telefone" required>
    </div>

    <div class="form-group">
        <label for="example-number-input" class="form-control-label">Gênero</label>
        <select class="form-control" id="genero" name="genero">
            <option>Prefiro não declarar</option>
            <option>Masculino</option>
            <option>Feminino</option>
            <option>Não-Binário</option>
        </select>
    </div>

    <div class="form-group">
        <label for="example-number-input" class="form-control-label">Tamanho de camisa</label>
        <select class="form-control" id="tamanho_camisa" name="tamanho_camisa">
            <option>--</option>
            <option>P</option>
            <option>M</option>
            <option>G</option>
            <option>GG</option>
            <option>XGG</option>
        </select>
    </div>

    <div class="form-group">
        <label for="exampleFormControlTextarea1">Chave GPG (fingerprints)</label>
        <textarea class="form-control" id="gpg_keys" placeholder="1234 5678 90AB CDEF 0000 1111 2222 3333 4444 5555" rows="2" name="gpg_keys"></textarea>
        <small>Opcional. Uma fingerprint por linha, se você quiser participar da sessão de assinatura de chaves. Aparecerá em crachás e uma folha de anotações de chave, sem verificação feita pelos organizadores da conferência.</small>
    </div>

    <h4>Datas</h4>

    <div class="form-group">
        <label for="example-datetime-local-input" class="form-control-label">Chegada</label>
        <input class="form-control" type="date" value="2023-05-25" id="datetime_chegada" name="datetime_chegada">
    </div>

    <div class="form-group">
        <label for="example-datetime-local-input" class="form-control-label">Saída</label>
        <input class="form-control datepicker" type="date" value="2023-05-27" id="datetime_saida" name="datetime_saida">
    </div>

    <hr/>

    <div>
        <div class="form-check form-switch mt-3">
            <input class="form-check-input" type="checkbox" id="covid_19" name="covid_19" required>
            <label class="form-check-label" for="flexSwitchCheckDefault">Estou totalmente vacinado contra o COVID-19.</label>
        </div>
        <small>Você está totalmente vacinado se tiver completado o curso completo de vacinação, pelo menos 2 semanas antes da chegada ao local da conferência.</small>
    </div>

    <div class="form-check form-switch mt-3">
        <input class="form-check-input" type="checkbox" id="codigo_de_conduta" name="codigo_de_conduta" required>
        <label class="form-check-label" for="flexSwitchCheckDefault">Li e prometo cumprir o <a href="/codigo-de-conduta" target="_blank" rel="noopener noreferrer">código de conduta.</a></label>
    </div>

    <hr/>

    <div class="mt-5">
        <input type="submit" class="btn btn-primary" value="Submeter Inscrição">
    </div>

</form>

{% else %}

#### A inscrição na {{ site.title }} ainda não está aberta.

{% endif %}
