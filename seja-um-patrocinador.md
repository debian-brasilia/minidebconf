---
layout: minidebconf
title: Seja um Patrocinador
---

Estamos buscando empresas ou entidades que queiram patrocinar a {{ site.title }} {{ site.year }}.

Se você tem interesse, ou tem contato com organizações que possam ajudar, por favor, envie um email para kanashiro@debian.org e {{ site.email }}.

Você pode fazer o download do nosso [plano de patrocínio](https://people.debian.org/~kanashiro/misc/minidebconf-brasilia-2023-patrocinadores.pdf) para saber informações mais detalhadas.
