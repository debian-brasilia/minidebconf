---
layout: minidebconf
title: Brasília
---


Em junho de 1892 a Missão Cruls começou com o objetivo de demarcar a área da futura capital do país, essa missão durou 7 meses e contou com uma equipe de médicos, geólogos e botânicos. A equipe deveria fazer levantamentos topográficos, climáticos, geológicos, sobre os recursos materiais da região e sobre como era a fauna e a flora. O resultado da Missão foi a demarcação do primeiro quadrilátero que seria construída a nova capital do país.

Em 1956 já com uma nova remarcação do local, o presidente da República na época **Juscelino Kubitschek** deu início ao projeto. Para a organização, a Novacap (Companhia Urbanizadora da Nova Capital) lançou o "Concurso Nacional do Planto Piloto da Nova Capital do Brasil".

O projeto vencedor do Concurso foi o do Arquiteto e Urbanista **Lucio Costa** que foi entregue desenhado à lápis e partiu do encontro de 2 eixos que cruzavam-se em um ângulo reto, fazendo o sinal da cruz.

Os eixos foram divididos em "Eixo Monumental" que abrigaria os prédios públicos, sendo os Federais no lado leste e os Locais no lado oeste e o "Eixo Rodoviário", que foi um pouco arqueado, levaria as áreas residenciais divididas entre Asa Sul e Asa Norte e subdivididas em Superquadras.

Na intersecção dos eixos estaria a Rodoviária e próximo à ela os setores bancários e comerciais. Com o urbanismo aprovado, Oscar Niemeyer foi escolhido para fazer os monumentos da nova capital, como o Congresso Nacional, os Palácios da Alvorada e do Planalto, a Catedral, entre outros. Oscar Niemeyer junto com Burle Marx e Athos Bulcão compuseram boa parte da paisagem de Brasília.


<div class="embed-responsive embed-responsive-4by3">
    <div id="carouselExampleIndicators" class="carousel slide embed-responsive-item" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="6"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="7"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="8"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="9"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="10"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="11"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="12"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="13"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="14"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block img-fluid" src="assets/img/brasilia/img-1.jpg" alt="">
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid " src="assets/img/brasilia/img-2.jpg" alt="">
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid " src="assets/img/brasilia/img-3.jpg" alt="">
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid " src="assets/img/brasilia/img-4.jpg" alt="">
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid " src="assets/img/brasilia/img-5.jpg" alt="">
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid " src="assets/img/brasilia/img-6.jpg" alt="">
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid " src="assets/img/brasilia/img-7.jpg" alt="">
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid " src="assets/img/brasilia/img-8.jpg" alt="">
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid " src="assets/img/brasilia/img-9.jpg" alt="">
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid " src="assets/img/brasilia/img-10.jpg" alt="">
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid " src="assets/img/brasilia/img-11.jpg" alt="">
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid " src="assets/img/brasilia/img-12.jpg" alt="">
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid " src="assets/img/brasilia/img-13.jpg" alt="">
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid " src="assets/img/brasilia/img-14.jpg" alt="">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Anterior</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Próximo</span>
        </a>
    </div>
</div>

---


## Mapa

<div class="embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-48.00270080566406%2C-15.871690500497143%2C-47.776107788085945%2C-15.714768429051942&amp;layer=mapnik" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=13/-15.7932/-47.8894">View Larger Map</a></small>
</div>
