---
layout: minidebconf
title: Patrocinadores
---

---

# Ouro

[![](/assets/img/sponsors/globo.svg){: width="250" height="100"}](https://www.globo.com/)
[![](/assets/img/sponsors/pencil.svg){: width="250"}](https://pencillabs.com.br/)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
[![](/assets/img/sponsors/policorp.png)](https://policorp.com.br/)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
[![](/assets/img/sponsors/toradex.svg){: width="250"}](https://www.toradex.com/)

---

## Prata

[![](/assets/img/sponsors/4linux.png){: width="150"}](https://4linux.com.br/)

---

### Apoiadores

[![](/assets/img/sponsors/finatec.png){: width="150"}](https://www.finatec.org.br)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
[![](/assets/img/sponsors/ictl.png){: width="150"}](https://www.ictl.org.br/)
