---
layout: minidebconf
title: Datas Importantes
---


<div class="card">
  <div class="table-responsive">
    <table class="table align-items-center mb-0">
      <thead>
        <tr>
          <th class="text-uppercase text-dark text-xxs font-weight-bolder opacity-7">Data</th>
          <th class="text-uppercase text-dark text-xxs font-weight-bolder opacity-7 ps-2">Descrição</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <h6 class="text-xs mb-0 ">{{ site.registration-open-date }}</h6>
          </td>
          <td>
            <p class="text-xs text-black mb-0">Abertura das inscrições para participação e submissão de palestras</p>
          </td>
        </tr>
        <tr>
          <td>
            <h6 class="text-xs mb-0 ">{{ site.submission-close-date }}</h6>
          </td>
          <td>
            <p class="text-xs text-black mb-0">Encerramento das inscrições para submissão de palestras</p>
          </td>
        </tr>
        <tr>
          <td>
            <h6 class="text-xs mb-0 ">{{ site.submission-results-date }}</h6>
          </td>
          <td>
            <p class="text-xs text-black mb-0">Divulgação das palestras selecionadas</p>
          </td>
        </tr>
        <tr>
          <td>
            <h6 class="text-xs mb-0 ">{{ site.start }} de {{ site.month }} de {{ site.year }}</h6>
          </td>
          <td>
            <p class="text-xs text-black mb-0">Início da {{ site.title }}</p>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
