---
layout: minidebconf
title: Index
---

# {{ site.title }} {{ site.year }}

Bem-vindo(a) ao site da **{{ site.title }} {{ site.year }}**  que aconteceu nos dias **{{ site.mdc-start-day }} e {{ site.mdc-end-day }}** de **{{ site.month }}** na **{{ site.mdc-event-location }}**, e no dia **{{ site.mdc-hacking-day }} ({{ site.mdc-hacking-day-weekday }})** no **{{ site.hacking-event-location }}**. Veja mais informações.

<!-- Você pode realizar a sua inscrição gratuitamente <a href="/inscricao">aqui</a>. -->

<hr>

<b>Gravações (<i itle="Vídeo"  data-toggle="tooltip" data-placement="top" class="fa fa-video-camera" aria-hidden="true"></i>) e slides (<i ata-toggle="tooltip" data-placement="top" title="Slides" class="fa fa-cloud-download" aria-hidden="true"></i>) das palestras estão disponíveis na página <a href="/programacao">programação</a>.

<hr>

<div class="row">
  <div class="col-12 col-md-8 col-sm-12 mt-2">
    <!-- <h3>Live <i class="fa fa-circle" style="color: red; font-size: 25px" aria-hidden="true"></i></h3>
    <div class="embed-responsive embed-responsive-16by9">
      <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/iOBXOG_rc2w" title="MiniDebConf Brasília 2023" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
    </div> -->
    <!-- <iframe width="100%" height="315" src="https://www.youtube.com/embed/iOBXOG_rc2w" title="MiniDebConf Brasília 2023" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe> -->
    <!-- <hr> -->
    <h3>Tabela de Conteúdos</h3>
    <ul>
        <li><a href="/sobre">Sobre</a></li>
        <li><a href="/programacao">Programação</a></li>
        <!-- <li><a href="/inscricao">Inscrição</a></li> -->
        <!-- <li><a href="/videos">Vídeos</a></li> -->
        <li><a href="/datas-importantes">Datas Importantes</a></li>
        <li><a href="/local">Local</a></li>
        <li><a href="/brasilia">Brasília</a></li>
        <li><a href="/patrocinadores">Patrocinadores</a></li>
        <li><a href="/organizadores">Organizadores</a></li>
        <!-- <li><a href="/voluntarios">Voluntários</a></li> -->
        <li><a href="/codigo-de-conduta">Código de Conduta</a></li>
      <!-- <li>Evento
        <ul>
          <li><a href="/sobre">Sobre</a></li>
          <li><a href="/programacao">Programação</a></li>
          <li><a href="/inscricao">Inscrição</a></li>
          <li><a href="/videos">Vídeos</a></li>
          <li><a href="/datas-importantes">Datas Importantes</a></li>
          <li><a href="/local">Local</a></li>
          <li><a href="/brasilia">Brasília</a></li>
          <li><a href="/codigo-de-conduta">Código de Conduta</a></li>
          <li><a href="/organizadores">Organizadores</a></li>
          <li><a href="/voluntarios">Voluntários</a></li>
          <li><a href="/hospedagem-solidaria">Hospedagem Solidária</a></li>
          <li><a href="/certificados">Certificados</a></li>
        </ul>
      </li> -->
      <!-- <li>Patrocínio
        <ul>
          <li><a href="/patrocinadores">Patrocinadores</a></li>
          <li><a href="/seja-um-patrocinador">Seja um Patrocinador</a></li>
        </ul>
      </li> -->
      <!-- <li>Programação
        <ul>
          <li><a href="/programacao">Programação</a></li>
          <li><a href="/datas-importantes">Datas Importantes</a></li>
          <li><a href="/submissao-de-atividades">Submissão de Atividades</a></li>
          <li><a href="/dicas-para-submissao">Dicas para Submissão</a></li>
          <li><a href="/voluntarios">Voluntários</a></li>
        </ul>
      </li> -->
      <li><a href="/contatos">Contatos</a></li>
    </ul>
    <hr>
  </div>
  <div class="col-12 col-md-4 col-sm-12">
    <h3>Patrocinadores</h3>
    <div class="card card-frame bg-secondary shadow border-5">
      <div class="card-header">
        Ouro <i class="ni ni-money-coins ml-3" style="color: #FAA300"></i>
      </div>
      <div class="card-body">
        <a href="https://pencillabs.com.br/"><img src="assets/img/sponsors/pencil.svg" style="width:150px; padding: 15px;"></a>
        <a href="https://www.globo.com/" class="pt-2"><img src="assets/img/sponsors/globo.svg" style="width:100px; padding: 15px;"></a>
        <a href="https://policorp.com.br/" class="pt-2"><img src="assets/img/sponsors/policorp.png" style="width:120px; padding: 15px;"></a>
        <a href="https://www.toradex.com" class="pt-2"><img src="assets/img/sponsors/toradex.svg" style="width:150px; padding: 15px;"></a>
      </div>
    </div>
    <div class="card card-frame bg-secondary shadow border-5 mt-3">
      <div class="card-header">
        Prata <i class="ni ni-money-coins ml-3" style="color: #9b9b9b"></i>
      </div>
      <div class="card-body">
	      <a href="https://4linux.com.br/"><img src="assets/img/sponsors/4linux.png" style="width:120px; padding: 15px;"></a>
      </div>
    </div>
    <div class="card card-frame bg-secondary shadow border-5 mt-3">
      <div class="card-header">
        Apoiadores
      </div>
      <div class="card-body">
	      <a href="https://www.finatec.org.br"><img src="assets/img/sponsors/finatec.png" style="width:130px; padding: 15px;"></a>
	      <a href="https://www.ictl.org.br/"><img src="assets/img/sponsors/ictl.png" style="width:150px; padding: 15px;"></a>
      </div>
    </div>
  </div>
</div>
