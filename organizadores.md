---
layout: minidebconf
title: Organizadores
---

- Debian Brasília
- Debian Brasil

### Apoio

- Universidade de Brasília (UnB)
- {{ site.mdc-event-location }}
- Instituto para Conservação de Tecnologias Livres (ICTL)
