

var url_string = window.location.href
var path_name = window.location.pathname
var search = decodeURIComponent(window.location.search).substring(1).split('&')
var url = new URL(url_string);

var error = url.searchParams.get("error");
var success = url.searchParams.get("success");

if (success) {
    document.getElementById("article_alert").innerHTML = `<div class="alert alert-success alert-dismissible fade show" role="alert"><span class="alert-text"><strong></strong> ${success}<button type="button" class="close" data-dismiss="alert" aria-label="Fechar"><span aria-hidden="true">&times;</span></button></div>`;
}

if (error) {
    document.getElementById("article_alert").innerHTML = `<div class="alert alert-danger alert-dismissible fade show" role="alert"><span class="alert-text"><strong></strong> ${error}<button type="button" class="close" data-dismiss="alert" aria-label="Fechar"><span aria-hidden="true">&times;</span></button></div>`;

    if (path_name == '/inscricao') {
        for (let i = 0; i < search.length; i++) {
            key = search[i].split('=')[0];
            value = search[i].split('=')[1];
            if (key == 'error' || key == 'covid_19' || key == 'codigo_de_conduta')
                continue

            input = document.getElementById(key);

            if (input) {
                input.value = value;
            }
        }
    }
}