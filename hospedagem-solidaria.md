---
layout: minidebconf
title: Hospedagem Solidária
---

Hospedagem solidária
A Comunidade Debian quer dar uma ajuda para quem precisa de um cantinho para ficar durante MiniDebConf Curitiba 2018 que acontecerá de 11 a 14 de abril na UTFPR.

Assim, criamos 2 tópicos no fórum para que você possa oferecer ou pedir um quarto (cama, sofá, etc) durante a MiniDebConf.

Não há coisa melhor que aproveitar um evento próximo à pessoas que compartilham dos mesmo valores!

Acesse o fórum:

- <a href="http://debianbrasil.org.br/forum/ofereco-hospedagem-minidebconf-brasilia-2023">Ofereço hospedagem</a>
- <a href="=http://debianbrasil.org.br/forum/preciso-de-hospedagem-minidebconf-brasilia-2023">Preciso de hospedagem</a>